#!/bin/bash

set -e

helper_dir="$(realpath -s "$(dirname "$0")")"
kernel_dir="$(readlink -f "$(dirname "${helper_dir}")")"
build_dir="${kernel_dir}/.output"
image_dir="${helper_dir}"

cd "${kernel_dir}"

. "${helper_dir}/config"
. "${helper_dir}/deviceinfo"

export KBUILD_VERBOSE=
export KBUILD_OUTPUT="${build_dir}"
export ARCH="${kernel_arch}"
export CROSS_COMPILE="${compiler_prefix}"
make() {
	make_cmd=(/usr/bin/make CC="${ccache_path} ${CROSS_COMPILE}${c_compiler_name}")
	[ "$*" = "clean" ] && {
		rm -f "${kernel_dir}/compile_commands.json" &>/dev/null
		"${make_cmd[@]}" "$@"
	} || {
		bear_cmd=(bear --output "${kernel_dir}/compile_commands.json")
		[ -s "${kernel_dir}/compile_commands.json" ] && {
			bear_cmd+=(--append)
		} || {
			rm -f "${kernel_dir}/compile_commands.json" &>/dev/null
			"${make_cmd[@]}" clean
		}
		bear_cmd+=(--)
		[ -n "$NO_BEAR" ] && bear_cmd=()
		"${bear_cmd[@]}" "${make_cmd[@]}" "$@"
		sed -re "s|\"${CROSS_COMPILE}${c_compiler_name}\"|\"clang\"|g" -i compile_commands.json
	}
}

[ ! -f "${build_dir}/.config" ] && {
	mkdir -p "${build_dir}"
	cp -v "${kernel_dir}/arch/${kernel_arch}/configs/${kernel_defconfig}_defconfig" "${build_dir}/.config"
}

[ $# -gt 0 ] && {
	make "$@"
	exit
}

grep -sq '^CONFIG_CMDLINE_FORCE=y' .output/.config \
	&& sed -re "s|^(CONFIG_CMDLINE=).*$|\1\"${deviceinfo_kernel_cmdline}\"|g" -i "${build_dir}/.config"
make -j$(nproc) all

mkbi_par=(
	--ramdisk "${image_dir}/initramfs"
	--cmdline "${deviceinfo_kernel_cmdline}"
	--base "${deviceinfo_flash_offset_base}"
	--pagesize "${deviceinfo_flash_pagesize}"
	--kernel_offset "${deviceinfo_flash_offset_kernel}"
	--ramdisk_offset "${deviceinfo_flash_offset_ramdisk}"
	--second_offset "${deviceinfo_flash_offset_second}"
	--tags_offset "${deviceinfo_flash_offset_tags}"
	--header_version "${deviceinfo_header_version}"
	--output "${image_dir}/boot.img"
	--id
)

case "$dtb_attach" in
kernel)
	cat \
		"${build_dir}/arch/${kernel_arch}/boot/Image.gz" \
		"${build_dir}/arch/${kernel_arch}/boot/dts/${device_dts}.dtb" \
		> "${image_dir}/vmlinuz"
	mkbi_par+=(--kernel "${image_dir}/vmlinuz")
	;;

boot_img)
	mkbi_par+=(
		--kernel "${build_dir}/arch/${kernel_arch}/boot/Image.gz"
		--dtb "${build_dir}/arch/${kernel_arch}/boot/dts/${device_dts}.dtb"
		--dtb_offset "dtb_offset"
	)
	;;

*)
	echo "ERROR: don't know where to place dtb"
	;;
esac

echo "mkbootimg ${mkbi_par[@]}"
mkbootimg "${mkbi_par[@]}"

e="$(echo -e "\e")"
cat <<EOF
$e[1;39mAll done... now flash boot.img:
$e[1;32mfastboot flash boot ${image_dir}/boot.img && fastboot reboot
$e[1;39mor
$e[1;33mfastboot flash recovery ${image_dir}/boot.img && fastboot reboot
$e[m
EOF
