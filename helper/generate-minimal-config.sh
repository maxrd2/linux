#!/bin/bash

set -e

helper_dir="$(realpath -s "$(dirname "$0")")"
kernel_dir="$(readlink -f "$(dirname "${helper_dir}")")"
build_dir="${kernel_dir}/.output"
image_dir="${helper_dir}"

cd "${kernel_dir}"

. "${helper_dir}/config"
. "${helper_dir}/deviceinfo"

cfg=(
	--file "${build_dir}/.config"
	--enable CONFIG_TTY
	--enable CONFIG_SERIAL_8250
	--enable CONFIG_SERIAL_8250_CONSOLE
	--enable CONFIG_SERIAL_OF_PLATFORM

	--enable CONFIG_SMP
	--enable CONFIG_IRQ_WORK
	--enable CONFIG_PRINTK

	--enable CONFIG_FB_EARLYCON
	--disable CONFIG_EFI_EARLYCON
)
for opt in "$@"; do
	cfg+=(--enable "$opt")
done

kmake() {
	"${helper_dir}/build.sh" "$@"
}

kmake mrproper
kmake allnoconfig
${kernel_dir}/scripts/config "${cfg[@]}"
yes '' | kmake oldconfig

cp -v "${build_dir}/.config" "${kernel_dir}/arch/${kernel_arch}/configs/${kernel_defconfig}_defconfig"
