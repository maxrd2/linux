#!/bin/bash

set -e

helper_dir="$(realpath -s "$(dirname "$0")")"

cd "${helper_dir}/ramfs"
find . | cpio -o -H newc | gzip -c > "$helper_dir/initramfs"

e="$(echo -e "\e")"
cat <<EOF
$e[1;39mAll done...:
$e[1;32minitramfs compressed to "$helper_dir/initramfs"
$e[m
EOF
