#!/bin/bash

set -e

helper_dir="$(realpath -s "$(dirname "$0")")"

rm -rf "${helper_dir}/ramfs"
mkdir "${helper_dir}/ramfs"
cd "${helper_dir}/ramfs"
gunzip -c "$helper_dir/initramfs" | cpio -i

e="$(echo -e "\e")"
cat <<EOF
$e[1;39mAll done...:
$e[1;32minitramfs extracted to $PWD
$e[m
EOF
