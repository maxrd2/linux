#!/bin/bash

set -e

cd "$(dirname "$0")"
helper/build.sh "$@"
