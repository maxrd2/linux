#!/bin/bash

cd "$(dirname "$0")"

sed -r -e "s|(<value [^>]*\"ProjectExplorer.BuildConfiguration.BuildDirectory\"[^>]*>).*(</value>)|\1%{ActiveProject:Path}/.output\2|g" \
	-i linux-kernel.creator.user

find * -name \*.c -or -name \*.h -or -name \*.S | sort \
	| grep -Pv '^(arch/([b-z].*|arm\b|arc|alpha)|tools)' \
	> linux-kernel.files
