## Kernel porting helper

Steps for initial kernel porting of unsupported android devices.
I've followed these to build a working kernel for my HTC Desire 728G (Mediatek MT6753).

The device doesn't have any kernel sources anywhere, so I had to build kernel from soruces of similar MT6753 based device.

The Early framebuffer console code was merged from [currently (2022-07-24) out-of-tree driver](https://github.com/Kiciuk/linux/commit/40db7f372fb46fb207b66e77e34b8e2fb177f00f) by [Nergzd723](https://wiki.postmarketos.org/wiki/User:Nergzd723).
Actual patches were applied from [here](https://lists.sr.ht/~postmarketos/upstreaming/patches/34257).

To make the above patches work on pre-6.0 kernels some tweaks were required and can be found in this repository.


### Install required tools

Instructions for installing package dependencies are Arch Linux specific, you'll have to figure yourself how to install arm64/aarch64 cross compiler on your distribution.
```sh
# install aarch64 cross compiler, git, dtc (device tree compiler), android-tools (adb, fastboot, mkbootimage) possibly other things are missing
pacman -S aarch64-linux-gnu-gcc git dtc android-tools
# for pre 4.10 kernels you need gcc5 aarch64 cross compiler from AUR
pacaur -S aarch64-linux-gnu-gcc5
```


### Checkout kernel and helper repository

```sh
# clone kernel you want to build
git clone <URL to git kernel sources> kernel-sources
# git clone --reference /home/max/projects/android-hacking/linux-mainline --branch v4.0 https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git kernel-sources
cd kernel-sources

git checkout <branch you want to build>

# add this repository as new remote
git remote add mainline-helper git@gitlab.com:maxrd2/linux.git
git fetch

# create new branch
git checkout -b porting

# cherry-pick helper scripts
git cherry-pick helper
```


### Configure environment

Edit default configuration file: `helper/config`

Edit deviceinfo file: `helper/deviceinfo`

Documentation on deviceinfo contents and help on figuring offsets etc can be found in [postmarketOS wiki](https://postmarketos.org/deviceinfo)


### Apply early framebuffer patches

At the moment there are branches that merge cleanly to v3.18 and v4.0 kernels.
They should work with higher kernel versions too.
```sh
cd kernel-sources
git branch -r --list mainline-helper/earlyfb/v*

git cherry-pick KERNEL_VERSION..mainline-helper/earlyfb/KERNEL_VERSION
# git cherry-pick v3.18..mainline-helper/earlyfb/v3.18
# git cherry-pick v4.0..mainline-helper/earlyfb/v4.0
```

If you prefer there are work branches for htc728g, you can cherry-pick those and exclude
commits whose description is starting with "*** "
```sh
cd kernel-sources
git branch -r --list mainline-helper/htc728g/earlyfb/v*
git cherry-pick v5.6..mainline-helper/htc728g/earlyfb/v5.6
# skip conflicts for commits whose description starts with '*** '
git cherry-pick --skip
```


### Test kernel build

Build helper script will use `kernel-source/.output` directory for all build output. That way it can be easily packaged into postmarketOS.

If you don't already have `kernel-source/arch/{kernel_arch}/configs/{kernel_defconfig}_defconfig` file you can run following to create minimal config with Mediatek SoC and early framebuffer support:
```sh
cd kernel-sources
helper/generate-minimal-config.sh CONFIG_ARCH_MEDIATEK # you can add more CONFIG_ entries - early framebuffer console is always added
git add arch/arm64/configs/mt6753_defconfig
git commit -m '*** defconfig'
```

If you don't already have `arch/{kernel_arch}/boot/dts/{device_dts}.dts` file you can copy `helper/minimal.dts` there.
Make sure you update the `arch/{kernel_arch}/boot/dts/{subdir}/Makefile` eg:
```patch
--- a/arch/arm64/boot/dts/mediatek/Makefile
+++ b/arch/arm64/boot/dts/mediatek/Makefile
@@ -1,4 +1,5 @@
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8173-evb.dtb
+dtb-$(CONFIG_ARCH_MEDIATEK) += mt6753-htc-a50cmg_dwg.dtb

 always         := $(dtb-y)
 subdir-y       := $(dts-dirs)
```

```sh
git add arch/arm64/boot/dts/
git commit -m '*** dts'
```

Compile the kernel:
```sh
cd kernel-sources
helper/build.sh
```

Flash the kernel/boot.img:
```sh
# you can flash it system boot partition
fastboot flash boot {kernel-sources}/helper/boot.img
# or to recovery partition
fastboot flash recovery {kernel-sources}/helper/boot.img
fastboot reboot
```


### QtCreator project

There is also a QtCreator project that allows you to open the kernel sources in IDE and easily analyze the code and jump through files.
Very handy for kernel noob like me :)


#### New QtCreator versions support build_commands.json projects

In QtCreator open `build_commands.json` as project.
Set build directory to `%{sourceDir}/.output`.
Add "custom process" build step and set build command to `%{sourceDir}/helper/build.sh`.
Add "custom process" clean step and set clean command to `%{sourceDir}/helper/build.sh` and arguments to `clean`.


#### Old QtCreator versions support custom projects

To set it up simply execute `kernel-sources/linux-kernel.files.sh` and open the `kernel-sources/linux-kernel.creator` project in QtCreator.

Please note that kernel sources are big, and clangd indexer can eat several GB of RAM.

Whenever you add/remove files to tree you have to execute `kernel-sources/linux-kernel.files.sh` to update project as qtcreator/clangd won't detect new files on their own.

To manage include directories, defines and compiler flags edit these files:
```
linux-kernel.cflags
linux-kernel.config
linux-kernel.includes
```
