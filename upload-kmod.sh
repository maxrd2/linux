#!/bin/bash

set -e

cd "$(dirname "$0")"

rm -rf dist
mkdir dist
./build.sh INSTALL_MOD_PATH="$PWD/dist" DEPMOD=/doesnt/exist modules_install
kmod_dir="$(basename "$(ls "$PWD/dist/lib/modules")")"
rsync -av --delete "$PWD/dist/lib/modules/${kmod_dir}/" "root@172.16.42.1:/lib/modules/${kmod_dir}"

ssh root@172.16.42.1 depmod
